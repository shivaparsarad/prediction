# Prediction

Predict Average minute Audience

Preprocessing and data prepration for used dataset was critical.
some columns are different between test and train dataset, so first these columns should detect and drop.

    for col in data.columns:
    if col not in (test_data.columns):
        data.drop(col,axis=1,inplace=True)
some columns like "Episode" and "Name of episode" have lots of values, we droped them.
about "hour_start" and "hour_end", thiere information is embeded in other columns(Start
_time and End_time), and they are not able to new valable information to prediction, so they dropped.

"Season" column was dropped, beacuse when we use month information, season gives no new information.

"Date", "End time" and "Start time" features include data about date and time, and some data like 
month are cyclic, the proper handling of such features involves representing the cyclical features 
as (x,y) coordinates on a circle.We map each cyclical variable onto a circle such that the lowest value for 
that variable appears right next to the largest value.


data set has numerical and categorical data,but predict a coninues value, so we should find a good solution to convert numerical data to categorical.
different method , such as dummy variable, Onehotencoding, label encoding, and some tricky methods. 
Label encoding is simply converting each value in a column to a number, this make some problem for prediction.
in this project, first categorical columns are transformed and then dummy variables was applied to them. it prevents duplicate columns production.

    for col in var_to_encode:
        data[col] = le.fit_transform(data[col])
    data = pd.get_dummies(data, columns=var_to_encode)
    print data.isnull().sum()

    
 for prediction, RandomForestRegressor, xgboost, DecisionTreeRegressor  are implemented. 
one problem with our dataset after convert categorical data to numerical  is the large number of columns that results in long time for traing, so using 
some feature selection methods is reasonable. PCA is used for dimension reduction, after applying PCA, the new representation of data was feeded to
selected prediction methods. improvment was not significant. For feature selection, correlation analysis is done and those feature with correlation
higher than a thereshold were selected as final features and models are trained with this new subset of features. DecisionTreeRegressor had good result 
with selected features. Random forest regressor colud be used as prediction and feature selection method, it could determine the importance of 
features that enable models to train just by importance features. 

The evaluation metric was Mean absolute error.
based on multiple run on different fraction of dataset, XGboost with subset of features selected by random forest and DecisionTreeRegressor
showed better results than others.The error was about 18.


