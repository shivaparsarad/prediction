import pandas as pd
from sklearn.tree import DecisionTreeRegressor
from collections import Counter
import numpy as np
import xgboost as xgb
from xgboost.sklearn import XGBClassifier
from sklearn.model_selection import train_test_split
import time

from sklearn.metrics import accuracy_score
from sklearn import metrics
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler

def accuracy_of_training(y_pred,y_test):
    comparision=np.where(y_pred==y_test,"correct","incorrect")
    counting=list(comparision).count("correct")
    print (comparision)
    print (counting)
    accuracy=counting/float(len(y_pred)) # how many correct from all
    return accuracy


def none(train_data,col):
     z = Counter(list(train_data[col]))
     sort = sorted(z.items(), key=lambda x: x[1], reverse=True)
     max_iteration = sort[0][0]
     return max_iteration


def pre(data):



    data.drop("Season",axis=1,inplace=True)
    data.drop("hour_start",axis=1,inplace=True)
    data.drop("hour_end",axis=1,inplace=True)
    data.drop("Episode",axis=1,inplace=True)
    data.drop("Name of episode",axis=1,inplace=True)



    def miss_val(df,col):
            z = Counter(list(df[col]))
            sort = sorted(z.items(), key=lambda x: x[1], reverse=True)
            max_iteration = sort[0][0]
            df[col] = df[col].fillna(max_iteration)
            return df[col]



    def dummy_var(col,data):
            just_dummies = pd.get_dummies(data[col])
            step_1 = pd.concat([data, just_dummies], axis=1)
            step_1.drop([col,just_dummies.columns[0]], inplace=True, axis=1)
            return step_1

    def total_seconds(col):
           second=data[col].dt.second
           minute=data[col].dt.minute
           hour=data[col].dt.hour
           for i in range(len(second)):
               data["x"] = int(hour[i]) * 3600 + int(minute[i]) * 60 + int(second[i])
           return data["x"]

    for col in data.columns:
        if col not in ("Start_time","End_time","Date"):
            if data[col].isnull().any().any():
                     data[col]=miss_val(data,col)


    data["Date"] = pd.to_datetime(data.Date)
    data["Start_time"] = pd.to_datetime(data.Start_time)
    data["End_time"] = pd.to_datetime(data.End_time)
    print ('start time', time.ctime())
    print data.isnull().sum()
    for i in range(data.shape[0]):

        if pd.isnull(data.loc[i]["Start_time"]):
            data.loc[i, "Start_time"] = data.loc[i - 1, "End_time"]
        if pd.isna(data.loc[i]["End_time"]):
                data.loc[i, "End_time"] = data.loc[i, "Start_time"]

    #print data.isnull().sum() # to realize which columns have null value except start-time,end-time and date

    #data["Name of episode"]=miss_val(data,"Name of episode")

    print (time.ctime())

    seconds_in_day = 24*60*60
    data["Time_End"] = total_seconds("End_time")
    data["Time_Start"] = total_seconds("Start_time")
    data.drop('x', axis=1, inplace=True)

    data['sin_time_End'] = np.sin(2 * np.pi * data.Time_End / seconds_in_day)
    data['cos_time_End'] = np.cos(2 * np.pi * data.Time_End / seconds_in_day)
    data.drop("Time_End", axis=1, inplace=True)

    data['sin_time_Start'] = np.sin(2 * np.pi * data.Time_Start / seconds_in_day)
    data['cos_time_Start'] = np.cos(2 * np.pi * data.Time_Start / seconds_in_day)
    data.drop("Time_Start", axis=1, inplace=True)

    data['mnth_sin_End'] = np.sin((data.End_time.dt.month - 1) * (2. * np.pi / 12))
    data['mnth_cos_End'] = np.cos((data.End_time.dt.month - 1) * (2. * np.pi / 12))

    data['day_sin_End'] = np.sin((2 * np.pi) / 30 * data.End_time.dt.day)
    data['day_cos_End'] = np.cos((2 * np.pi) / 30 * data.End_time.dt.day)

    data['mnth_sin_Start'] = np.sin((data.Start_time.dt.month - 1) * (2. * np.pi / 12))
    data['mnth_cos_Start'] = np.cos((data.Start_time.dt.month - 1) * (2. * np.pi / 12))

    data['day_sin_Start'] = np.sin((2 * np.pi) / 30 * data.Start_time.dt.day)
    data['day_cos_Start'] = np.cos((2 * np.pi) / 30 * data.Start_time.dt.day)

    data.drop("Date", axis=1, inplace=True)
    data.drop("End_time", axis=1, inplace=True)
    data.drop("Start_time", axis=1, inplace=True)
    print (time.ctime())

    """ for col in categorical_columns:
           data[col] = data[col].astype('category')
           data[col] = data[col].cat.codes"""
    print data.isnull().sum()
    from sklearn.preprocessing import LabelEncoder
    le = LabelEncoder()
    var_to_encode = ["Station", "Channel Type", "Year", "Day of week", "Name of show",
                     "Genre", "First time or rerun", "# of episode in the season", "Movie?",
                     "Game of the Canadiens during episode?"]
    for col in var_to_encode:
        data[col] = le.fit_transform(data[col])
    data = pd.get_dummies(data, columns=var_to_encode)
    print data.isnull().sum()

    return data



print (time.ctime())



data= pd.read_csv("/Users/shiva/Desktop/data_processed.csv")
"""for col in data.columns:

    print str(col), data[col].unique().shape"""
data=data.sample(frac=0.001,replace=False).reset_index(drop=True)
target=data["Average Minute Audience_total"]

data.drop('Average Minute Audience_total',axis=1,inplace=True)

test_data=pd.read_csv("/Users/shiva/Desktop/test.csv")
for col in test_data.columns:
    if test_data[col].isnull().all():
        max=none(data,col)
        test_data.loc[:,col]=max

for col in data.columns:
    if col not in (test_data.columns):
        data.drop(col,axis=1,inplace=True)

#print data["Episode"].unique().shape
#print data.shape
#print data["Channel Type"].unique().shape
#print data["Station"].unique().shape
#print data["Day of week"].unique().shape



data=pre(data)
#from sklearn.preprocessing import MinMaxScaler
#my_col=data.columns
#scaler = MinMaxScaler()
#scaled_values = scaler.fit_transform(data)
#data.loc[:,:] = scaled_values



from sklearn.preprocessing import StandardScaler

def normalize(df):
    result = df.copy()
    for feature_name in df.columns:
        max_value = df[feature_name].max()
        min_value = df[feature_name].min()
        if max_value>min_value:
            for value in df[feature_name].values:
                if value!=min_value:
                    result[feature_name] = (df[feature_name] - min_value) / (max_value - min_value)
    return result

new_data=normalize(data)

test_data=test_data.dropna(how='any')
my_data = new_data.dropna(how='any')





datar=data.copy()
datar["target"]=target



#Extract 50000 instances from the dataset
from sklearn.ensemble import RandomForestRegressor


x_train,x_test,y_train,y_test=train_test_split(datar,target,test_size=0.2)
clf = RandomForestRegressor()
clf.fit(x_train,y_train)

#Train the classifier and calculate the training time



clf.fit(x_train,y_train)

#Lets Note down the model training time



pre = clf.predict(x_test)
predictions = clf.predict(x_test)
# Performance metrics
errors = abs(predictions - y_test)

print('Average absolute error:', round(np.mean(errors), 2), 'degrees.')


datar=datar.drop("target",axis=1,inplace=True)
feature_list = list(data.columns)
# Get numerical feature importances
importances = list(clf.feature_importances_)
# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
# Print out the feature and importances
#print feature_importances
# Cumulative importances
sorted_features = [importance[0] for importance in feature_importances]
sorted_importances = [importance[1] for importance in feature_importances]
cumulative_importances = np.cumsum(sorted_importances)
xz=[]
print feature_importances
for feature in feature_importances:
    if feature[1]>0:
        xz.append(feature[0])
print xz


# Create training and testing sets with only the important features

important_train_features = x_train[xz]
important_test_features = x_test[xz]
clf.fit(important_train_features, y_train)
# Make predictions on test data
predictions =clf.predict(important_test_features)
# Performance metrics
errors = abs(predictions - y_test)
print('Average absolute error on selected features:', round(np.mean(errors), 2), 'degrees.')

regressorwiths = DecisionTreeRegressor(random_state=0)

regressorwiths.fit(important_train_features,y_train)
#regressor.fit(data, target)
y_preds = regressorwiths.predict(important_test_features)

print "Mean Absolute Error  by selected features of randomforest ", mean_absolute_error(y_test,y_preds)


predictor= xgb.XGBClassifier(learning_rate=0.1,max_depth=5,subsample=0.8)
print time.ctime()
predictor.fit(important_train_features, y_train)
print time.ctime()
y_predicts = predictor.predict(important_test_features)
print time.ctime()
print "XGB on selected features: ",metrics.mean_absolute_error(y_test,y_predicts)






#print my_data.isnull().any().any()
from sklearn.decomposition import PCA


"""pca = PCA(n_components=4,svd_solver='full')
principalComponents = pca.fit_transform(my_data)
principalDf = pd.DataFrame(data = principalComponents)"""

new_train=my_data.copy()

new_train['target']=target
cor = new_train.corr()
cor_target = abs(cor["target"])

#Selecting highly correlated features
relevant_features = cor_target[cor_target>0.1]

df=new_train[relevant_features.keys()]
df.drop('target',axis=1,inplace=True)

x_train,x_test,y_train,y_test=train_test_split(df,target,test_size=0.2)
regressor = DecisionTreeRegressor(random_state=0)

regressor.fit(x_train,y_train)
#regressor.fit(data, target)
y_pred = regressor.predict(x_test)


#print ("Accuracy on data: ",accuracy_of_training(y_pred,y_test))

print "Mean Absolute Error with relevant feature extracted of co", mean_absolute_error(y_test,y_pred)

predictor= xgb.XGBClassifier(learning_rate=0.1,max_depth=5,subsample=0.8)
print time.ctime()
predictor.fit(x_train, y_train)
print time.ctime()
y_predict = predictor.predict(x_test)
print time.ctime()
print "XGB after co analysis", metrics.mean_absolute_error(y_test,y_predict)






x_train,x_test,y_train,y_test=train_test_split(my_data,target,test_size=0.2)
regressor = DecisionTreeRegressor(random_state=0)

regressor.fit(x_train,y_train)
#regressor.fit(data, target)
y_pred2 = regressor.predict(x_test)

print "Mean Absolute Error of simple", mean_absolute_error(y_test,y_pred2)
from sklearn.decomposition import PCA

"""pca = PCA()
X_train = pca.fit_transform(x_train)
X_test = pca.transform(x_test)
explained_variance = pca.explained_variance_ratio_"""
#print explained_variance  # 476


x_train,x_test,y_train,y_test=train_test_split(my_data,target,test_size=0.2)

pca = PCA(n_components=476)
x_train = pca.fit_transform(x_train)
x_test = pca.transform(x_test)

regressor = DecisionTreeRegressor(random_state=0)

regressor.fit(x_train,y_train)
#regressor.fit(data, target)
y_predicted = regressor.predict(x_test)


#print ("Accuracy on data: ",accuracy_of_training(y_pred,y_test))

print "Mean Absolute Error", mean_absolute_error(y_test,y_predicted)



predictor= xgb.XGBClassifier(learning_rate=0.1,max_depth=5,subsample=0.8)
print time.ctime()
predictor.fit(x_train, y_train)
print time.ctime()
y_predict = predictor.predict(x_test)
print time.ctime()
print metrics.mean_absolute_error(y_test,y_predict)

#xgaccuracy=accuracy_score(y_test.values, y_predict)
#print "AUC Score (Train): %f" % metrics.roc_auc_score(y_test, y_predict)
#print(xgaccuracy)
"""feat_imp = pd.DataFrame(predictor.feature_importances_,x_train).sort_values(ascending=False)
print feat_imp"""

#lr = LinearRegression()
#lr.fit(x_train, y_train)
#lr.fit(data,target)
#y_pred_lr=lr.predict(x_test)
#print (y_pred_lr)
#print ("Accuracy", accuracy_of_training(y_pred_lr,y_test))



#test_data=test_data.sample(frac=0.5,replace=False).reset_index(drop=True)

test_data=pre(test_data)
test_data=normalize(test_data)
test_col=test_data.columns
#test_data[[my_col]] = StandardScaler().fit_transform(test_data[[my_col]])
#test_data[["Station","Channel Type","Year","Day of week","Name of show","Name of episode",
 #                       "Genre","First time or rerun","# of episode in the season","Movie?","Game of the Canadiens during episode?"]] = StandardScaler().fit_transform(test_data[["Station","Channel Type","Year","Day of week","Name of show","Name of episode",
  #                       "Genre","First time or rerun","# of episode in the season","Movie?","Game of the Canadiens during episode?"]])


#print(test_data.isnull().sum())
#print(data.isnull().sum())

y_pred_test1 = regressor.predict(test_data)
print ("result of DecisionTreeRegressor: ",y_pred_test1)
"""
y_pred_test2=lr.predict(test_data)
print ("Result of regression: ", y_pred_test2)

"""

